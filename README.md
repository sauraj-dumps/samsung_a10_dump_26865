## raven-user 12 SQ1D.220205.003 8069835 release-keys
- Manufacturer: samsung
- Platform: universal7884b
- Codename: a10
- Brand: samsung
- Flavor: cipher_a10-user
- Release Version: 12
- Id: SP2A.220305.012
- Incremental: eng.root.20220320.124152
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/raven/raven:12/SQ1D.220205.003/8069835:user/release-keys
- OTA version: 
- Branch: raven-user-12-SQ1D.220205.003-8069835-release-keys
- Repo: samsung_a10_dump_26865


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
